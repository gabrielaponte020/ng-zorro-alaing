import { Component, Input, OnInit } from '@angular/core';
import { PaginateResult } from '../interfaces/PaginateResult';

type alingCollumn = 'center' | 'left' | 'right';

export interface TableColumn {
  key: string;
  size?: string;
  align?: alingCollumn;
  [key: string]: any;
}

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
})
export class TableComponent implements OnInit {
  @Input() data: PaginateResult<any>;
  @Input() limit: number;
  @Input() total: number;
  @Input() columns: TableColumn[] = [];
  @Input() actions: any[] = [];
  @Input() loading = false;

  constructor() {}
  ngOnInit() {}

  renderColumn(data: any, header: any) {
    return data[header.key];
  }
}
