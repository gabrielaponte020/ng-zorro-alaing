import { ICodeableConcept, IHumanName } from '@pegasi/hl7';

export class CodeableConcept implements ICodeableConcept {
  get active() {
    return this.active;
  }
  set active(value: boolean) {
    this.active = value;
  }
  get displays() {
    return this.displays;
  }
  set displays(value: any) {
    this.displays = value;
  }
}

export class Utils {
  static getCodeableConceptValue(codeableConcept: ICodeableConcept, language: string) {
    return codeableConcept && codeableConcept.displays && codeableConcept.displays.find((item) => item.language === language)
      ? codeableConcept.text
      : '';
  }

  static setHumanName(name: string, lastname?: string): IHumanName[] {
    const object = {};
    const humanName = object as IHumanName;
    humanName.use = 'usual';
    humanName.text = name;
    humanName.family = lastname || '';
    return [humanName];
  }

  static getCodeableConceptTranslationText(codeableConcept: ICodeableConcept, language: string): string {
    return codeableConcept.displays.find((item) => item.language === language).value;
  }
}
