export interface PaginateResult<T> {
  docs: Array<T>;
  totalDocs: any;
  limit: any;
  page?: any;
  totalPages?: any;
  hasNextPage?: boolean;
  hasPrevPage?: boolean;
  nextPage?: number;
  pagingCounter?: number;
  prevPage?: number;
}

export interface PaginateDto {
  skip: number;
  limit: number;
  select?: string;
  filter?: string;
}
