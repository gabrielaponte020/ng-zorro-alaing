import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { PaginateDto } from 'src/app/shared/interfaces/PaginateResult';

export abstract class BaseService<T> {
  constructor(public http: HttpClient, public url: string, public complement?: string) {}
  paginate(params?: PaginateDto, id?: string): Observable<T[]> {
    const parameters: any = params ? params : { filter: 'active:true' };
    return this.http.get<T[]>(`${this.url}/paginate`, { params: parameters });
  }
}
