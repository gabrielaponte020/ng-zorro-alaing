import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable()
export class CommonsService {
  constructor(private readonly http: HttpClient) {}

  languages() {
    return this.http.get(environment.languages).pipe(
      map((result) => {
        return result;
      }),
    );
  }

  countries() {
    return this.http.get(environment.countries).pipe(
      map((result) => {
        return result;
      }),
    );
  }

  gender() {
    return this.http.get(environment.gender).pipe(
      map((result) => {
        return result;
      }),
    );
  }

  civilStatus() {
    return this.http.get(environment.civilStatus).pipe(
      map((result) => {
        return result;
      }),
    );
  }

  profession() {
    return this.http.get(environment.profession).pipe(
      map((result) => {
        return result;
      }),
    );
  }

  nationality() {
    return this.http.get(environment.nationality).pipe(
      map((result) => {
        return result;
      }),
    );
  }

  handDominance() {
    return this.http.get(environment.handDominance).pipe(
      map((result) => {
        return result;
      }),
    );
  }

  identificationType() {
    return this.http.get(environment.identificationType).pipe(
      map((result) => {
        return result;
      }),
    );
  }
}
