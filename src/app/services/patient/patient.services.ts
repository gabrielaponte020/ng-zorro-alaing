import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IPatient } from '@pegasi/hl7';
import { PaginateDto, PaginateResult } from 'src/app/shared/interfaces/PaginateResult';
import { environment } from 'src/environments/environment';
import { BaseService } from '../commons/base.service';

@Injectable()
export class PatientService extends BaseService<IPatient> {
  constructor(public http: HttpClient) {
    super(http, environment.patient);
  }

  async getPaginate(payload: PaginateDto): Promise<PaginateResult<IPatient>> {
    const data = (await this.paginate(payload).toPromise()) as any;

    return data;
  }
}
