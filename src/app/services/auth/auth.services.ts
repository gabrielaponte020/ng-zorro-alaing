import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { StartupService } from '@core';
import { DA_SERVICE_TOKEN, ITokenService } from '@delon/auth';
import * as jwt_decode from 'jwt-decode';

import { Role } from 'src/app/models/roles';
import { environment } from 'src/environments/environment';

export interface IUser {
  email: string;
  type: string;
  _id: string;
  userName: string;
}

export interface IAuthUser {
  _id: string;
  firstLogin: boolean;
  permits: [];
  active: boolean;
  username: string;
  email: string;
  licenseKey: string;
  role: string;
  iat: number;
  exp: number;
}

export interface LoginDto {
  email: string;
  password: string;
}


@Injectable()
export class AuthService {
  role: Role;

  constructor(
    private readonly http: HttpClient,
    @Inject(DA_SERVICE_TOKEN) private tokenService: ITokenService,
    private startupSrv: StartupService,
    private router: Router,
  ) {}

  async login(payload: LoginDto): Promise<void> {
    const { token, user, refreshToken } = await this.http.post(`${environment.url}/auth/login`, payload).toPromise() as any;
    this.tokenService.set({ token, user: jwt_decode(token), refreshToken });
    this.startupSrv.load().then(() => {
      let url = this.tokenService.referrer.url || '/';
      if (url.includes('/passport')) {
        url = '/';
      }
      this.router.navigateByUrl(url);
    });
  }
}
