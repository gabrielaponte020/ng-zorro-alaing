import { Component, OnInit } from '@angular/core';
import { CommonsService } from '../../services/commons/commons.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
})
export class DashboardComponent implements OnInit {
  constructor(private readonly commonsService: CommonsService) {}

  ngOnInit() {
    this.getAllEnumerables();
  }

  getAllEnumerables(): void {
    Promise.all([
      this.getCountries(),
      this.getGender(),
      this.getCivilStatus(),
      this.getProfession(),
      this.getNationality(),
      this.getHandDominance(),
      this.getIdentificationType(),
      this.getLanguages(),
    ])
      .then((values) => {})
      .catch((reason) => {});
  }

  getIdentificationType(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.commonsService.identificationType().subscribe((res) => {
        localStorage.setItem('identificationType', JSON.stringify(res));
        resolve();
      });
      reject();
    });
  }

  getCountries(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.commonsService.countries().subscribe((res) => {
        localStorage.setItem('countries', JSON.stringify(res));
        resolve();
      });
      reject();
    });
  }

  getGender(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.commonsService.gender().subscribe((res) => {
        localStorage.setItem('gender', JSON.stringify(res));
        resolve();
      });
      reject();
    });
  }

  getCivilStatus(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.commonsService.civilStatus().subscribe((res) => {
        localStorage.setItem('civilStatus', JSON.stringify(res));
        resolve();
      });
      reject();
    });
  }

  getProfession(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.commonsService.profession().subscribe((res) => {
        localStorage.setItem('profession', JSON.stringify(res));
        resolve();
      });
      reject();
    });
  }

  getNationality(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.commonsService.nationality().subscribe((res) => {
        localStorage.setItem('nationality', JSON.stringify(res));
        resolve();
      });
      reject();
    });
  }

  getHandDominance(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.commonsService.handDominance().subscribe((res) => {
        localStorage.setItem('handDominance', JSON.stringify(res));
        resolve();
      });
      reject();
    });
  }

  getLanguages(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.commonsService.languages().subscribe((res) => {
        localStorage.setItem('languages', JSON.stringify(res));
        resolve();
      });
      reject();
    });
  }
}
