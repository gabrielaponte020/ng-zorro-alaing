import { NgModule } from '@angular/core';
import { SharedModule } from '@shared';
import { NzAffixModule } from 'ng-zorro-antd/affix';
import { NzDatePickerModule } from 'ng-zorro-antd/date-picker';
import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { NzPageHeaderModule } from 'ng-zorro-antd/page-header';
import { NzSpaceModule } from 'ng-zorro-antd/space';
import { NzUploadModule } from 'ng-zorro-antd/upload';
import { TableComponent } from '../shared/common-table/table.component';
// single pages
import { CallbackComponent } from './callback/callback.component';
// dashboard pages
import { DashboardComponent } from './dashboard/dashboard.component';
import { UserLockComponent } from './passport/lock/lock.component';
// passport pages
import { UserLoginComponent } from './passport/login/login.component';
import { UserRegisterResultComponent } from './passport/register-result/register-result.component';
import { UserRegisterComponent } from './passport/register/register.component';
import { PatientCreateComponent } from './patient/create/patient-create.component';
import { PatientListComponent } from './patient/list/patient-list.component';
import { SafeHtmlPipe } from './patient/list/pipes/patiente-sanitazer-dom.component';
import { PatientComponent } from './patient/patient.component';
import { RouteRoutingModule } from './routes-routing.module';

const COMPONENTS = [
  DashboardComponent,
  // passport pages
  UserLoginComponent,
  UserRegisterComponent,
  UserRegisterResultComponent,
  // single pages
  CallbackComponent,
  UserLockComponent,
  PatientComponent,
  TableComponent,
];
const COMPONENTS_NOROUNT = [];

@NgModule({
  imports: [
    SharedModule,
    RouteRoutingModule,
    NzPageHeaderModule,
    NzSpaceModule,
    NzDatePickerModule,
    NzUploadModule,
    NzAffixModule,
    NzLayoutModule,
  ],
  declarations: [...COMPONENTS, ...COMPONENTS_NOROUNT, PatientListComponent, PatientCreateComponent, SafeHtmlPipe],
})
export class RoutesModule {}
