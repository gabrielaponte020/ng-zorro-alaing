import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { _HttpClient } from '@delon/theme';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzModalService } from 'ng-zorro-antd/modal';
import { AuthService } from '../../../services/auth/auth.services';

@Component({
  selector: 'passport-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less'],
})
export class UserLoginComponent {
  constructor(
    fb: FormBuilder,
    modalSrv: NzModalService,
    private router: Router,
    public http: _HttpClient,
    public msg: NzMessageService,
    private authService: AuthService,
  ) {
    this.form = fb.group({
      email: ['', [Validators.required, Validators.pattern('[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}')]],
      password: ['', Validators.required]
    });
    modalSrv.closeAll();
  }

  get email() {
    return this.form.controls.email;
  }
  get password() {
    return this.form.controls.password;
  }
  form: FormGroup;
  error = '';
  loading: boolean;

  async submit() {
    this.error = '';
    this.email.markAsDirty();
    this.email.updateValueAndValidity();
    this.password.markAsDirty();
    this.password.updateValueAndValidity();
    if (this.email.invalid || this.password.invalid) {
        return;
      }

    try {
        this.loading = true;
        await this.authService.login({ email: this.email.value, password: this.password.value });
        this.loading = false;
      } catch (e) {
        this.loading = false;
        this.msg.error(e);
      }
  }

}
