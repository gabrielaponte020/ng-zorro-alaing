import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { IPatient } from '@pegasi/hl7';
import { PatientService } from '../../services/patient/patient.services';
import { PaginateDto, PaginateResult } from '../../shared/interfaces/PaginateResult';

@Component({
  selector: 'app-patient',
  templateUrl: './patient.component.html',
})
export class PatientComponent implements OnInit {
  loading = false;
  show = false;
  patients: PaginateResult<IPatient> = {
    docs: [],
    limit: 10,
    totalDocs: 0,
  };
  civilStatus = [];
  nationalities = [];
  identificationTypesOrigin = [];
  genders = [];

  constructor(private service: PatientService, public translate: TranslateService) {}

  async ngOnInit() {
    await this.getAll({
      limit: 20,
      skip: 1,
    });
    this.civilStatus = JSON.parse(localStorage.getItem('civilStatus'));
    this.nationalities = JSON.parse(localStorage.getItem('nationality'));
    this.identificationTypesOrigin = JSON.parse(localStorage.getItem('identificationType'));
    this.genders = JSON.parse(localStorage.getItem('gender'));
  }

  async getAll({ filter = 'active:eq:true', limit = 10, select = 'name identifier active', skip = 1 }: PaginateDto) {
    const data = await this.service.getPaginate({
      filter,
      limit,
      select,
      skip,
    });
    data.docs = data.docs.map((d) => this.setAttrListPatient(d));
    this.patients = data;
  }

  visibleDrawe(visible: boolean) {
    this.show = visible;
  }

  setAttrListPatient(item: IPatient) {
    return {
      ...item,
      fullName: `${item.name[0].text} ${item.name[0].family}`,
      identification: this.getUsualIdentifier(item.identifier),
      medicalIdentification: this.getOfficialIdentifier(item.identifier),
    };
  }

  getUsualIdentifier(identifier) {
    const usualData = identifier.find((type) => type.use === 'usual');
    return usualData !== undefined
      ? `${
          usualData.identifierType.displays.find(
            (item) => item.language === this.translate.store.currentLang || this.translate.store.defaultLang,
          )?.value ?? ''
        } ${usualData.value}`
      : 'N/A';
  }
  getOfficialIdentifier(identifier) {
    return identifier.find((type) => type.use === 'official').value;
  }
}
