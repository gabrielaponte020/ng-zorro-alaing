import {
  IAddress,
  IAttachment,
  ICodeableConcept,
  ICodeableConceptModel,
  ICommunication,
  IContact,
  IContactPoint,
  ICustomerOrganizationModel,
  IHumanName,
  IIdentification,
  ILabor,
  IOrganization,
  IPatient,
  IPractitioner,
} from '@pegasi/hl7';

import { Utils } from '../../../shared/utils/utils';

export class Patient implements IPatient {
  user: string;
  admission: { status: boolean; encounter: any };
  _id: string;
  name: IHumanName[];
  identifier: IIdentification[];
  birthDate: Date;
  birthCountry: ICodeableConcept;
  deceasedBoolean: boolean;
  deceasedDateTime: Date;
  deceasedCountry: ICodeableConcept;
  resourceType: 'Patient';
  active: boolean;
  telecom: IContactPoint[];
  gender: ICodeableConcept;
  address: IAddress[];
  maritalStatus: ICodeableConcept;
  profession: ICodeableConcept;
  nationality: ICodeableConcept;
  notes: string;
  geoReference: string;
  labor: ILabor;
  handedness: ICodeableConcept;
  referedBy: string;
  multipleBirthBoolean: boolean;
  multipleBirthInteger: number;
  photo: IAttachment[];
  contact: IContact[];
  communication: ICommunication[];
  generalPractitioner: IPractitioner[];
  managingOrganization: IOrganization;
  insuranceCompanies: [{ name: string; number: string }];
  link: Array<any>;
  residenceCountry: ICodeableConcept;
  payer: {
    payerType: ICodeableConceptModel;
    customerOrganization: ICustomerOrganizationModel;
  };

  licenseKey: string;
  fiscalRegistry;
  fromJSON(json: any): Patient {
    const item = new Patient();
    Object.keys(json).forEach((key) => {
      item[key] = json[key];
    });
    Object.assign(Object.create(Patient.prototype), item);
    return item;
  }

  getLicenseKey() {
    return this.licenseKey;
  }
  setLicenseKey(value: string) {
    this.licenseKey = value;
  }

  getId(): string {
    return this._id;
  }
  setId(value: string) {
    this._id = value;
  }

  getName(): IHumanName[] {
    return this.name;
  }
  setName(value: IHumanName[]) {
    this.name = value;
  }
  getPayerType(): ICodeableConceptModel {
    if (this.payer && this.payer.payerType) {
      return this.payer.payerType;
    }
  }
  getCompanyClient(): ICustomerOrganizationModel {
    if (this.payer && this.payer.customerOrganization) {
      return this.payer.customerOrganization;
    }
  }
  setPayerType(payer: ICodeableConceptModel, organization: ICustomerOrganizationModel) {
    this.payer = {
      payerType: payer,
      customerOrganization: organization,
    };
  }

  getHumanName(): string {
    return this.name[0] ? this.name[0].text : null;
  }
  getLastName(): string {
    return this.name[0] ? this.name[0].family : null;
  }
  getIdentifier() {
    return this.identifier;
  }
  setIdentifier(value: IIdentification[]) {
    this.identifier = value;
  }

  getUsualIdentifierType(): ICodeableConcept {
    const identifier = this.identifier.find((item) => item.use === 'usual');
    return identifier !== undefined ? identifier.identifierType : null;
  }

  getUsualIdentifier(): string {
    const identifier = this.identifier.find((item) => item && item.use === 'usual');
    return identifier ? identifier.value : '';
  }

  getFiscalIdentifier(): string {
    const identifier = this.identifier.find((item) => item.identifierType && item.identifierType.text.toLowerCase() === 'fiscal-registry');
    return identifier ? identifier.value : '';
  }

  getRecordNumber(): string {
    const identifier = this.identifier.find((item) => item.identifierType.text.toLowerCase() === 'mr');
    return identifier ? identifier.value : '';
  }

  getResourceType() {
    return this.resourceType;
  }
  setResourceType(value: 'Patient') {
    this.resourceType = value;
  }

  getActive() {
    return this.active;
  }
  setActive(value: boolean) {
    this.active = value;
  }

  getTelecom() {
    return this.telecom;
  }
  setTelecom(value: IContactPoint[]) {
    this.telecom = value;
  }

  getEmail(): string {
    const email = this.telecom.find((item) => item.system === 'email');
    return email ? email.value : '';
  }

  getHomePhone(): string {
    const phone = this.telecom.find((item) => item.system === 'phone' && item.use === 'home');
    return phone ? phone.value : '';
  }

  getWorkPhone(): string {
    const phone = this.telecom.find((item) => item.system === 'phone' && item.use === 'work');
    return phone ? phone.value : '';
  }

  getCellphone(): string {
    const phone = this.telecom.find((item) => item.system === 'phone' && item.use === 'mobile');
    return phone ? phone.value : '';
  }

  getGender() {
    return this.gender;
  }
  setGender(value: ICodeableConcept) {
    this.gender = value;
  }

  getGenderString(language: string): string {
    return Utils.getCodeableConceptValue(this.gender, language);
  }

  getResidenceCountry() {
    return this.residenceCountry;
  }
  setResidenceCountry(value: ICodeableConcept) {
    this.residenceCountry = value;
  }

  getResidenceCountryString(language: string) {
    return Utils.getCodeableConceptValue(this.profession, language);
  }

  getBirthDate() {
    return this.birthDate;
    // this.converBirthDates( this.birthDate );
  }

  converBirthDates(date) {
    const today: Date = new Date();
    const birthDate: Date = new Date(date);
    let age: number = today.getFullYear() - birthDate.getFullYear();
    const month: number = today.getMonth() - birthDate.getMonth();
    const days: number = today.getDay() - birthDate.getDay();
    if (month < 0 || (month === 0 && today.getDate() < birthDate.getDate())) {
      age--;
    }
    return month;
  }

  setBirthDate(value: Date) {
    this.birthDate = value;
  }

  getDeceasedBoolean() {
    return this.deceasedBoolean;
  }
  setDeceasedBoolean(value: boolean) {
    this.deceasedBoolean = value;
  }

  getDeceasedDateTime() {
    return this.deceasedDateTime;
  }
  setDeceasedDateTime(value: Date) {
    this.deceasedDateTime = value;
  }

  getAddress() {
    return this.address;
  }
  setAddress(value: IAddress[]) {
    this.address = value;
  }

  getResidencialAddress(): string {
    const address = this.address.find((item) => item.use === 'home');
    return address ? address.text : '';
  }

  getPostalCode(): string {
    const address = this.address.find((item) => item.use === 'home');
    return address ? address.postalCode : '';
  }

  getMaritalStatus() {
    return this.maritalStatus;
  }
  setMaritalStatus(value: ICodeableConcept) {
    this.maritalStatus = value;
  }

  getMaritalStatusString(language: string): string {
    return Utils.getCodeableConceptValue(this.maritalStatus, language);
  }

  getMultipleBirthBoolean() {
    return this.multipleBirthBoolean;
  }
  setMultipleBirthBoolean(value: boolean) {
    this.multipleBirthBoolean = value;
  }

  getMultipleBirthInteger() {
    return this.multipleBirthInteger;
  }
  setMultipleBirthInteger(value: number) {
    this.multipleBirthInteger = value;
  }

  getPhoto() {
    return this.photo;
  }
  setPhoto(value: IAttachment[]) {
    this.photo = value;
  }

  getContact() {
    return this.contact;
  }
  setContact(value: IContact[]) {
    this.contact = value;
  }

  getMother() {
    return this.contact.find((item) => item.relationship && item.relationship.text === 'mother');
  }

  getFather() {
    return this.contact.find((item) => item.relationship && item.relationship.text === 'father');
  }

  getEmergencyContact() {
    return this.contact.find((item) => item.relationship && item.relationship.text === 'emergency-contact');
  }

  getRepresentative() {
    return this.contact.find((item) => item.relationship && item.relationship.text === 'representative');
  }

  setRepresentative(value: IContact, type: ICodeableConcept) {
    const object = {};
    const contact = object as IContact;
    contact.relationship = type;
    contact.name = value.name;
    contact.identifier = value.identifier;
    contact.address = value.address;
    contact.telecom = value.telecom;
    contact.insuranceCompanies = value.insuranceCompanies;
    return contact;
  }

  setMother(value: IContact, type: ICodeableConcept) {
    const object = {};
    const contact = object as IContact;
    contact.relationship = type;
    contact.identifier = value.identifier;
    contact.name = value.name;
    contact.address = value.address;
    contact.telecom = value.telecom;
    contact.insuranceCompanies = value.insuranceCompanies;
    return contact;
  }

  setFather(value: IContact, type: ICodeableConcept) {
    const object = {};
    const contact = object as IContact;
    contact.relationship = type;
    contact.identifier = value.identifier;
    contact.name = value.name;
    contact.address = value.address;
    contact.telecom = value.telecom;
    contact.insuranceCompanies = value.insuranceCompanies;
    return contact;
  }

  setEmergencyContact(value: IContact, type: ICodeableConceptModel) {
    const object = {};
    const contact = object as IContact;
    contact.relationship = type;
    contact.identifier = value.identifier;
    contact.name = value.name;
    contact.address = value.address;
    contact.telecom = value.telecom;
    return contact;
  }

  getEmergencyContactName() {
    return this.getEmergencyContact() ? this.getEmergencyContact().name.text : '';
  }

  getMotherName() {
    return this.getMother() ? this.getMother().name.text : '';
  }

  getFatherName() {
    return this.getFather() ? this.getFather().name.text : '';
  }

  getRepresentativeHumanName(): string {
    return this.getRepresentative() ? this.getRepresentative().name.text : '';
  }

  getEmergencyContactIdentificationType(): ICodeableConcept {
    const identifier = this.getEmergencyContact()
      ? this.getEmergencyContact().identifier.find((item) => item && item.use === 'usual')
      : null;
    return identifier ? identifier.identifierType : null;
  }

  getMotherIdentificationType(): ICodeableConcept {
    const identifier =
      this.getMother() && this.getMother().identifier ? this.getMother().identifier.find((item) => item && item.use === 'usual') : null;
    return identifier ? identifier.identifierType : null;
  }

  getFatherIdentificationType(): ICodeableConcept {
    const identifier =
      this.getFather() && this.getFather().identifier ? this.getFather().identifier.find((item) => item && item.use === 'usual') : null;
    return identifier ? identifier.identifierType : null;
  }

  getRepresentativeIdentificationType(): ICodeableConcept {
    const identifier =
      this.getRepresentative() && this.getRepresentative().identifier
        ? this.getRepresentative().identifier.find((item) => item && item.use === 'usual')
        : null;
    return identifier ? identifier.identifierType : null;
  }

  getEmergencyContactIdentification(): string {
    const identifier =
      this.getEmergencyContact() && this.getEmergencyContact().identifier
        ? this.getEmergencyContact().identifier.find((item) => item && item.use === 'usual')
        : null;
    return identifier ? identifier.value : '';
  }

  getMotherIdentification(): string {
    const identifier = this.getMother() ? this.getMother().identifier.find((item) => item && item.use === 'usual') : null;
    return identifier ? identifier.value : '';
  }

  getFatherIdentification(): string {
    const identifier = this.getFather() ? this.getFather().identifier.find((item) => item && item.use === 'usual') : null;
    return identifier ? identifier.value : '';
  }

  getRepresentativeIdentification(): string {
    const identifier = this.getRepresentative() ? this.getRepresentative().identifier.find((item) => item && item.use === 'usual') : null;
    return identifier ? identifier.value : '';
  }

  getEmergencyContactEmail(): string {
    const email = this.getEmergencyContact() ? this.getEmergencyContact().telecom.find((item) => item && item.system === 'email') : null;
    return email ? email.value : '';
  }

  getMotherEmail(): string {
    const email = this.getMother() ? this.getMother().telecom.find((item) => item && item.system === 'email') : null;
    return email ? email.value : '';
  }

  getFatherEmail(): string {
    const email = this.getFather() ? this.getFather().telecom.find((item) => item && item.system === 'email') : null;
    return email ? email.value : '';
  }

  getRepresentativeEmail(): string {
    const email = this.getRepresentative() ? this.getRepresentative().telecom.find((item) => item && item.system === 'email') : null;
    return email ? email.value : '';
  }

  getEmergencyContactHomePhone(): string {
    const phone = this.getEmergencyContact()
      ? this.getEmergencyContact().telecom.find((item) => item && item.system === 'phone' && item.use === 'home')
      : null;
    return phone ? phone.value : '';
  }

  getMotherHomePhone(): string {
    const phone = this.getMother() ? this.getMother().telecom.find((item) => item && item.system === 'phone' && item.use === 'home') : null;
    return phone ? phone.value : '';
  }

  getFatherHomePhone(): string {
    const phone = this.getFather() ? this.getFather().telecom.find((item) => item && item.system === 'phone' && item.use === 'home') : null;
    return phone ? phone.value : '';
  }

  getRepresentativeHomePhone(): string {
    const phone = this.getRepresentative()
      ? this.getRepresentative().telecom.find((item) => item && item.system === 'phone' && item.use === 'home')
      : null;
    return phone ? phone.value : '';
  }

  getEmergencyMobilePhone(): string {
    const phone = this.getEmergencyContact()
      ? this.getEmergencyContact().telecom.find((item) => item && item.system === 'phone' && item.use === 'mobile')
      : null;
    return phone ? phone.value : '';
  }

  getMotherMobilePhone(): string {
    const phone = this.getMother()
      ? this.getMother().telecom.find((item) => item && item.system === 'phone' && item.use === 'mobile')
      : null;
    return phone ? phone.value : '';
  }

  getFatherMobilePhone(): string {
    const phone = this.getFather()
      ? this.getFather().telecom.find((item) => item && item.system === 'phone' && item.use === 'mobile')
      : null;
    return phone ? phone.value : '';
  }

  getRepresentativeWorkPhone(): string {
    const phone = this.getRepresentative()
      ? this.getRepresentative().telecom.find((item) => item && item.system === 'phone' && item.use === 'work')
      : null;
    return phone ? phone.value : '';
  }

  getRepresentativeCellPhone(): string {
    const phone = this.getRepresentative()
      ? this.getRepresentative().telecom.find((item) => item && item.system === 'phone' && item.use === 'mobile')
      : null;
    return phone ? phone.value : '';
  }

  getEmergencyContactResidencialAddress(): string {
    const address = this.getEmergencyContact() ? this.getEmergencyContact().address : null;
    return address ? address.text : '';
  }

  getMotherResidencialAddress(): string {
    const address = this.getMother() ? this.getMother().address : null;
    return address ? address.text : '';
  }

  getFatherResidencialAddress(): string {
    const address = this.getFather() ? this.getFather().address : null;
    return address ? address.text : '';
  }

  getRepresentativeResidencialAddress(): string {
    const address = this.getRepresentative() ? this.getRepresentative().address : null;
    return address ? address.text : '';
  }

  getEmergencyContactPostalCode(): string {
    const address = this.getEmergencyContact() ? this.getEmergencyContact().address : null;
    return address ? address.postalCode : '';
  }

  getRepresentativePostalCode(): string {
    const address = this.getRepresentative() ? this.getRepresentative().address : null;
    return address ? address.postalCode : '';
  }

  getMotherInsuranceCompany(): string {
    let company = '';
    if (this.getMother() && this.getMother().insuranceCompanies && this.getMother().insuranceCompanies.length > 0) {
      company = this.getMother().insuranceCompanies[0].name;
    }
    return company;
  }

  getFatherInsuranceCompany(): string {
    let company = '';
    if (this.getFather() && this.getFather().insuranceCompanies && this.getFather().insuranceCompanies.length > 0) {
      company = this.getFather().insuranceCompanies[0].name;
    }
    return company;
  }

  getRepresentativeInsuranceCompany(): string {
    let company = '';
    if (this.getRepresentative() && this.getRepresentative().insuranceCompanies && this.getRepresentative().insuranceCompanies.length > 0) {
      company = this.getRepresentative().insuranceCompanies[0].number;
    }
    return company;
  }

  getRepresentativeRecordNumber(): string {
    let record = '';
    if (this.getRepresentative() && this.getRepresentative().insuranceCompanies && this.getRepresentative().insuranceCompanies.length > 0) {
      record = this.getRepresentative().insuranceCompanies[0].number;
    }
    return record;
  }

  getCommunication() {
    return this.communication;
  }
  setCommunication(value: ICommunication[]) {
    this.communication = value;
  }

  getLanguage(language: string): string {
    // tslint:disable-next-line:no-shadowed-variable
    const languageArray = this.communication.find((item: any) => item.preferred);
    const item = languageArray ? languageArray.language : null;
    return Utils.getCodeableConceptValue(item, language);
  }

  setLanguage(value: ICodeableConcept) {
    this.communication = value
      ? [
          {
            language: value,
            preferred: true,
          },
        ]
      : [];
  }

  getGeneralPractitioner() {
    return this.generalPractitioner;
  }
  setGeneralPractitioner(value: IPractitioner[]) {
    this.generalPractitioner = value;
  }

  getGeneralPractitionerString(): string {
    let practitioner;
    if (this.generalPractitioner) {
      practitioner =
        this.generalPractitioner.length > 0 && this.generalPractitioner[0].name[0]
          ? this.generalPractitioner[0].name[0].text + ' ' + this.generalPractitioner[0].name[0].family
          : '';
    }
    return practitioner;
  }

  setGeneralPractitionerString(value: string) {
    const object = {};
    const practitioner = object as IPractitioner;
    practitioner.name = Utils.setHumanName(value);
    practitioner.address = [];
    practitioner.communication = [];
    practitioner.gender = { active: true, text: 'GENDER', coding: [], displays: [{ language: 'es', value: 'GENDER' }] };
    practitioner.identifier = [];
    if (this.generalPractitioner) {
      this.generalPractitioner.push(practitioner);
    } else {
      this.generalPractitioner = [practitioner];
    }
  }

  getManagingOrganization() {
    return this.managingOrganization;
  }
  setManagingOrganization(value: IOrganization) {
    this.managingOrganization = value;
  }

  getInsuranceCompany(): string {
    let company = '';
    if (this.insuranceCompanies && this.insuranceCompanies.length > 0) {
      company = this.insuranceCompanies[0].number;
    }
    return company;
  }

  setInsuranceCompany(value) {
    value
      ? (this.insuranceCompanies = [
          {
            name: value.name,
            number: value._id,
          },
        ])
      : // tslint:disable-next-line:no-unused-expression
        '';
  }

  getLink() {
    return this.link;
  }
  setLink(value: Array<any>) {
    this.link = value;
  }

  getNationality() {
    return this.nationality;
  }
  setNationality(value: ICodeableConcept) {
    this.nationality = value;
  }

  getNationalityString(language: string): string {
    return Utils.getCodeableConceptValue(this.nationality, language);
  }

  getBirthCountry() {
    return this.birthCountry;
  }
  setBirthCountry(value: ICodeableConcept) {
    this.birthCountry = value;
  }

  getBirthCountryString(language: string): string {
    return Utils.getCodeableConceptValue(this.birthCountry, language);
  }

  getDeceasedCountry() {
    return this.deceasedCountry;
  }
  setDeceasedCountry(value: ICodeableConcept) {
    this.deceasedCountry = value;
  }

  getDeceasedCountryString(language: string): string {
    return Utils.getCodeableConceptValue(this.deceasedCountry, language);
  }

  getReferedBy() {
    return this.referedBy;
  }
  setReferedBy(value: string) {
    this.referedBy = value;
  }

  getHandedness() {
    return this.handedness;
  }
  setHandedness(value: ICodeableConcept) {
    this.handedness = value;
  }

  getHandednessString(language: string): string {
    return Utils.getCodeableConceptValue(this.handedness, language);
  }

  getProfession() {
    return this.profession;
  }
  setProfession(value: ICodeableConcept) {
    this.profession = value;
  }

  getProfessionString(language: string): string {
    return Utils.getCodeableConceptValue(this.profession, language);
  }

  getLabor() {
    return this.labor;
  }
  setLabor(value: ILabor) {
    this.labor = value;
  }

  getLaborIdentification(): string {
    const identifier = this.labor ? this.labor.identifier : null;
    return identifier ? identifier.value : '';
  }

  getLaborIdentificationType(): ICodeableConcept {
    const identifier = this.labor ? this.labor.identifier : null;
    return identifier ? identifier.identifierType : null;
  }

  getLaborCompanyName(): string {
    return this.labor ? this.labor.name : '';
  }

  getLaborCargo(): string {
    return this.labor ? this.labor.cargo : '';
  }

  getLaborDepartment(): string {
    return this.labor ? this.labor.department : '';
  }

  getLaborAddress() {
    this.labor ? (this.labor.address = this.labor.address) : (this.labor = null);
    return this.labor ? this.labor.address : null;
  }

  getLaborPostalCode(): string {
    return this.getLaborAddress() ? this.getLaborAddress().postalCode : '';
  }

  getLaborAddressInfo(): string {
    return this.getLaborAddress() ? this.getLaborAddress().text : '';
  }

  getLaborContact() {
    this.labor ? (this.labor.contact = this.labor.contact) : (this.labor = null);
    return this.labor ? this.labor.contact[0].telecom : null;
  }

  getLaborEmail(): string {
    const email = this.getLaborContact() ? this.getLaborContact().find((item) => item.system === 'email') : null;
    return email ? email.value : '';
  }

  getLaborPhone(): string {
    const email = this.getLaborContact() ? this.getLaborContact().find((item) => item.system === 'phone' && item.use === 'work') : null;
    return email ? email.value : '';
  }

  getGeoReference() {
    return this.geoReference;
  }
  setGeoReference(value: string) {
    this.geoReference = value;
  }

  getNotes() {
    return this.notes;
  }
  setNotes(value: string) {
    this.notes = value;
  }
}
