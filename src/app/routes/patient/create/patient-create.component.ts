import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NzUploadFile } from 'ng-zorro-antd/upload';
import { Patient } from '../model/patient';

@Component({
  selector: 'app-create-patient',
  templateUrl: './patient-create.component.html',
})
export class PatientCreateComponent implements OnInit {
  @Input() show: boolean;
  @Output() closeDrawer = new EventEmitter<any>();
  @Input() civilStatus: any;
  @Input() identificationTypes: any;
  @Input() genders: any;
  validateForm!: FormGroup;
  patient: Patient;
  selectedValue = null;
  countries = [
    {
      text: 'Germany',
      value: 'germany',
    },
    {
      text: 'Venezuela',
      value: 'venezuela',
    },
    {
      text: 'Chile',
      value: 'chile',
    },
    {
      text: 'Colombia',
      value: 'colombia',
    },
    {
      text: 'Dominican Republic',
      value: 'dominicanRepublic',
    },
  ];
  dateDeathBoolean = false;
  customerOrganizations = [
    {
      text: 'Salud Total',
      value: 'saludTotal',
    },
  ];
  payerTypes = [
    {
      text: 'Personal',
      value: 'personal',
    },
    {
      text: 'Mother',
      value: 'mother',
    },
    {
      text: 'Father',
      value: 'father',
    },
    {
      text: 'Representative',
      value: 'representative',
    },
  ];
  profession = [
    {
      text: 'Teacher',
      value: 'teacher',
    },
    {
      text: 'Artist',
      value: 'artist',
    },
    {
      text: 'Baker',
      value: 'baker',
    },
    {
      text: 'Chef',
      value: 'chef',
    },
    {
      text: 'Other',
      value: 'other',
    },
  ];
  mainPractitioner = [
    {
      text: 'Jean Pierre',
      value: 'jeanPierre',
    },
    {
      text: 'Genesis Garcia',
      value: 'genesisGarcia',
    },
  ];
  dominance = [
    {
      text: 'Left',
      value: 'left',
    },
    {
      text: 'Rigth',
      value: 'rigth',
    },
    {
      text: 'Ambidextrous',
      value: 'ambidextrous',
    },
  ];
  date = null;
  language = [
    {
      text: 'Spanish',
      value: 'spanish',
    },
    {
      text: 'English',
      value: 'english',
    },
  ];
  fileList: NzUploadFile[] = [];
  previewImage: string | undefined = '';
  previewVisible = false;
  offsetTop = 10;

  constructor(private fb: FormBuilder) {}

  ngOnInit() {
    console.log(this.civilStatus, 'this.civilStatus');
    this.validateForm = this.fb.group({
      patient_photo: [''],
      patient_record_number: [null, [Validators.required]],
      patient_name: [null, [Validators.required]],
      patient_lastname: [null, [Validators.required]],
      patient_identification_type: [null, [Validators.required]],
      patient_identification: [null, [Validators.required, Validators.pattern('^([0-9]|-|\\.){1,13}$')]],
      patient_fiscal_identification: [null, Validators.pattern('^([0-9]|-|\\.){1,13}$')],
      patient_birthdate: [new Date(), [Validators.required]],
      patient_country_birth: [null],
      patient_nationality: [null],
      patient_date_deceased: [null],
      patient_country_deceased: [null],
      patient_gender: [null, [Validators.required]],
      patient_civil_status: [null, [Validators.required]],
      patient_payer_type: [null],
      patient_profession: [null],
      patient_country_residence: [null, [Validators.required]],
      patient_referred: [null],
      patient_main: [null],
      patient_email: [null, [Validators.email]],
      patient_home_phone: [null],
      patient_cellphone: [null],
      patient_work_phone: [null],
      patient_dominance: [null],
      patient_language: [null, [Validators.required]],
      patient_insurance: [null],
      patient_address: [null],
      patient_postal_code: [null, [Validators.min(0)]],
      patient_note: [null],
      /*
      patient_company_client_payer: [null],
      patient_georeferred: [this.patient.getGeoReference()],
      ,*/
    });
  }

  getBase64(file: File): Promise<string | ArrayBuffer | null> {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = (error) => reject(error);
    });
  }

  handlePreview = async (file: NzUploadFile) => {
    if (!file.url && !file.preview) {
      // tslint:disable-next-line:no-non-null-assertion
      file.preview = await this.getBase64(file.originFileObj!);
    }
    this.previewImage = file.url || file.preview;
    this.previewVisible = true;
    // tslint:disable-next-line
  };

  customRequest({ onSuccess }: any) {
    setTimeout(() => {
      onSuccess('ok');
    }, 0);
  }

  close(visible: boolean) {
    this.closeDrawer.emit(visible);
  }

  submitForm(): void {
    /*  for (const i in this.validateForm.controls) {
      this.validateForm.controls[i].markAsDirty();
      this.validateForm.controls[i].updateValueAndValidity();
    }*/
  }
}
