import { Component, Input, OnInit } from '@angular/core';
import { IPatient } from '@pegasi/hl7';
import { TableColumn } from '../../../shared/common-table/table.component';
import { PaginateResult } from '../../../shared/interfaces/PaginateResult';

enum PatientAction {
  detail = 'detail',
  edit = 'edit',
  deactivate = 'detail',
  history = 'history',
}

@Component({
  selector: 'app-patient-list',
  templateUrl: './patient-list.component.html',
})
export class PatientListComponent implements OnInit {
  @Input() patients: PaginateResult<IPatient>;
  @Input() loading: boolean;
  columns: TableColumn[] = [
    {
      name: 'Name',
      sortOrder: null,
      key: 'fullName',
    },
    {
      name: 'Identificación nacional',
      sortOrder: null,
      key: 'identification',
    },
    {
      name: 'No. de historia clínica',
      sortOrder: null,
      key: 'medicalIdentification',
    },
    {
      name: 'Estatus',
      sortOrder: null,
      key: 'active',
    },
  ];
  actions: any = [
    {
      title: 'Historial',
      action: this.handleActions,
      type: 'dropdown',
      rules: [],
      items: [
        {
          title: 'Detalle',
          rules: [],
          action: this.handleActions,
        },
        {
          title: 'Editar',
          rules: [],
          action: this.handleActions,
        },
        {
          title: 'Desactivar',
          rules: [],
          action: this.handleActions,
        },
      ],
    },
  ];

  constructor() {}

  ngOnInit() {}

  handleActions(data: IPatient) {}
}
