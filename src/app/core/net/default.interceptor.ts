import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponseBase } from '@angular/common/http';
import { Injectable, Injector } from '@angular/core';
import { Router } from '@angular/router';
import { DA_SERVICE_TOKEN, ITokenService } from '@delon/auth';
import { _HttpClient } from '@delon/theme';
import { environment } from '@env/environment';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { Observable, of, throwError } from 'rxjs';
import { catchError, mergeMap } from 'rxjs/operators';

const CODEMESSAGE = {
  200: 'El servidor devolvió con éxito los datos solicitados. ',
  201: 'Los datos nuevos o modificados tuvieron éxito. ',
  202: 'Una solicitud ha entrado en la cola de fondo (tarea asincrónica). ',
  204: 'Eliminar datos con éxito. ',
  400: 'Hubo un error en la solicitud enviada, el servidor no creó ni modificó datos. ',
  401: 'El usuario no tiene permiso (el token, el nombre de usuario y la contraseña son incorrectos). ',
  403: 'El usuario está autorizado, pero el acceso está prohibido. ',
  404: 'La solicitud enviada era para un registro inexistente y el servidor no funcionaba. ',
  406: 'El formato solicitado no está disponible. ',
  410: 'El recurso solicitado se elimina permanentemente y ya no se obtendrá. ',
  422: 'Al crear un objeto, se produjo un error de validación. ',
  500: 'Se produjo un error en el servidor, verifique el servidor. ',
  502: 'Error de puerta de enlace. ',
  503: 'El servicio no está disponible, el servidor está temporalmente sobrecargado o mantenido. ',
  504: 'La puerta de enlace expiró. ',
};

@Injectable()
export class DefaultInterceptor implements HttpInterceptor {
  constructor(private injector: Injector) {}

  private get notification(): NzNotificationService {
    return this.injector.get(NzNotificationService);
  }

  private goTo(url: string) {
    setTimeout(() => this.injector.get(Router).navigateByUrl(url));
  }

  private checkStatus(ev: HttpResponseBase) {
    if ((ev.status >= 200 && ev.status < 300) || ev.status === 401) {
      return;
    }

    const errortext = CODEMESSAGE[ev.status] || ev.statusText;
    this.notification.error(`Solicitud de error ${ev.status}: ${ev.url}`, errortext);
  }

  private handleData(ev: HttpResponseBase): Observable<any> {
    if (ev.status > 0) {
      this.injector.get(_HttpClient).end();
    }
    this.checkStatus(ev);

    // Operaciones comunes
    switch (ev.status) {
      case 200:
        // Manejo de errores a nivel empresarial, se supone que el resto tiene un formato de salida unificado (es decir, si existe un formato de datos correspondiente independientemente del éxito o no)
        // Por ejemplo, el contenido de la respuesta:
        //  Contenido de error：{ status: 1, msg: '非法参数' }
        //  Contenido correcto：{ status: 0, response: {  } }
        // Luego, el siguiente fragmento de código se puede aplicar directamente
        // if (event instanceof HttpResponse) {
        //     const body: any = event.body;
        //     if (body && body.status !== 0) {
        //         this.msg.error(body.msg);
        //         // Continúe lanzando errores e interrumpa todos los subsiguientes. Pipe、subscribe：
        //         // this.http.get('/').subscribe() No se dispara
        //         return throwError({});
        //     } else {
        //         // Cambia el contenido de `body` a` response` content, para la mayoría de las escenas ya no es necesario preocuparse por el código de estado del servicio
        //         return of(new HttpResponse(Object.assign(event, { body: body.response })));
        //         // O aún conserva el formato completo
        //         return of(event);
        //     }
        // }
        break;
      case 401:
        this.notification.error(`No ha iniciado sesión o el inicio de sesión ha caducado, vuelva a iniciar sesión.`, ``);
        (this.injector.get(DA_SERVICE_TOKEN) as ITokenService).clear();
        this.goTo('/passport/login');
        break;
      case 403:
      case 404:
      case 500:
        this.goTo(`/exception/${ev.status}`);
        break;
      default:
        if (ev instanceof HttpErrorResponse) {
          console.warn('Error desconocido, principalmente debido a backend que no admite CORS o configuración no válida', ev);
        }
        break;
    }
    if (ev instanceof HttpErrorResponse) {
      return throwError(ev);
    } else {
      return of(ev);
    }
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let url = req.url;
    if (!url.startsWith('https://') && !url.startsWith('http://')) {
      url = environment.SERVER_URL + url;
    }

    const newReq = req.clone({ url });
    return next.handle(newReq).pipe(
      mergeMap((event: any) => {
        if (event instanceof HttpResponseBase) {
          return this.handleData(event);
        }
        return of(event);
      }),
      catchError((err: HttpErrorResponse) => this.handleData(err)),
    );
  }
}
