---
componente: app-header
título: menú superior
---

Los componentes del menú superior permiten el ensamblaje de componentes bajo demanda en el directorio `components`.

## Lista de componentes

Nombre del componente | Descripción
---- | ------
`header-fullscreen` | Cambio de pantalla completa
`header-icon` | Icono de la aplicación
`header-langs` | Cambio de idioma
`header-notify` | Notificación de menú
`header-search` | Cuadro de búsqueda
`header-storage` | Borrar caché LocalStorage
`header-task` | Notificación de tarea
`header-theme` | Cambio de tema
`header-user` | Menú de usuario
