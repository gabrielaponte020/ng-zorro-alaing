import patient from './patient';
const buildTranslations = (data: any[]) => {
  const modules: any = {
    'en-US': {},
    'es-ES': {},
  };

  for (const moduleName of Object.keys(data) as any) {
    for (const lang of Object.keys(data[moduleName])) {
      modules[lang] = {
        ...modules[lang].translation,
        ...data[moduleName][lang],
      };
    }
  }

  return modules;
};

const translations = buildTranslations([
  patient,
]);

export default translations;
