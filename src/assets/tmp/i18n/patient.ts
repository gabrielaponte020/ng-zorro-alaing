
export default {
  'es-ES': {
    patient: {
      title: 'Paciente',
      createPatient: 'Crear paciente',
    }
  },
  'en-US': {
    patient: {
      title: 'Patient',
      createPatient: 'Create patient',
    }
  }
};
