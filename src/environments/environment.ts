// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
const url = 'http://demo2020.pegasi.io:81';

export const environment = {
  SERVER_URL: `./`,
  production: false,
  useHash: true,
  hmr: false,
  url,
  patient: `${url}/patients`,
  countries: `${url}/enumerables/countries/`,
  civilStatus: `${url}/enumerables/maritalStatus/`,
  languages: `${url}/enumerables/languages/`,
  gender: `${url}/enumerables/genders/`,
  handDominance: `${url}/enumerables/handDominances/`,
  nationality: `${url}/enumerables/nationalities/`,
  profession: `${url}/enumerables/professions/`,
  identificationType: `${url}/enumerables/identifierTypes/`,
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
